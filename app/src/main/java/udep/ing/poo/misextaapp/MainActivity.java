package udep.ing.poo.misextaapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.LinkedList;
import java.util.List;

import udep.ing.poo.misextaapp.db.Conexion;
import udep.ing.poo.misextaapp.db.Usuario;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    Button botonRegistrar;
    ListView listView1;

    Conexion conexion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        conexion = new Conexion(this);

        botonRegistrar = (Button)findViewById(R.id.button);
        botonRegistrar.setOnClickListener(this);

        listView1 = (ListView)findViewById(R.id.listView1);
        listView1.setOnItemClickListener(this);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getLista());
        listView1.setAdapter(adaptador);

    }


    @Override
    public void onClick(View view) {

        if (view.getId() == botonRegistrar.getId()) {

            Intent i = new Intent(this,MainActivity2.class);
            startActivity(i);

        }

    }
    List<Usuario> listaUsuarios;

    private List<String> getLista() {

        listaUsuarios = new LinkedList<Usuario>();
        List<String> listaNombres = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Usuario.CAMPO_ID, Usuario.CAMPO_NOMBRE, Usuario.CAMPO_TELEFONO, Usuario.CAMPO_FOTO};

        Cursor cursor = db.query(Usuario.TABLA_USUARIOS,campos,null,null,null,null,null);
        while (cursor.moveToNext()) {

            int id = cursor.getInt(0);
            String nombre = cursor.getString(1);
            String telefono = cursor.getString(2);

            Bitmap bitmap = null;
            if (cursor.getBlob(3)!=null) {
                byte[] bytes = cursor.getBlob(3);
                bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            }

            Usuario u = new Usuario(id,nombre,telefono,bitmap);
            listaUsuarios.add(u);

            listaNombres.add(u.getNombre());

        }

        return listaNombres;

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Intent intent = new Intent(this, MainActivity3.class);

        // envio un objeto completo en forma serializada
        Bundle extras = new Bundle();
        extras.putSerializable("usuario",listaUsuarios.get(i));
        intent.putExtras(extras);

        startActivity(intent);

    }
}