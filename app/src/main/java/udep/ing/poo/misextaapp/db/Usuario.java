package udep.ing.poo.misextaapp.db;

import android.graphics.Bitmap;

import java.io.Serializable;

public class Usuario implements Serializable {

    private int id;
    private String nombre;
    private String telefono;
    private transient Bitmap imagen;

    public Usuario(int id, String nombre, String telefono, Bitmap imagen) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.imagen = imagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Bitmap getImagen() {
        return imagen;
    }

    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }

    public static final String CREAR_TABLA_USUARIO = "CREATE TABLE usuarios (id INTEGER, nombre TEXT, telefono TEXT, foto BLOB)";
    public static final String DROPEAR_TABLA_USUARIO = "DROP TABLE IF EXISTS usuarios ";

    public static final String TABLA_USUARIOS = "usuarios";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_TELEFONO = "telefono";
    public static final String CAMPO_FOTO = "foto";


}
