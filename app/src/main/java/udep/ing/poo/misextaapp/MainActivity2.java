package udep.ing.poo.misextaapp;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import udep.ing.poo.misextaapp.db.Conexion;
import udep.ing.poo.misextaapp.db.Usuario;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    Button botonRegistrarUsuario;
    EditText insertId, insertNombre, insertTelefono;
    ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        botonRegistrarUsuario = (Button)findViewById(R.id.button2);
        botonRegistrarUsuario.setOnClickListener(this);

        insertId = (EditText)findViewById(R.id.insertId);
        insertNombre = (EditText)findViewById(R.id.insertNombre);
        insertTelefono = (EditText)findViewById(R.id.insertTelefono);

        imagen = (ImageView)findViewById(R.id.imageView);
        imagen.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == botonRegistrarUsuario.getId()) {

            Conexion conexion = new Conexion(this);
            SQLiteDatabase db = conexion.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(Usuario.CAMPO_ID, insertId.getText().toString());
            values.put(Usuario.CAMPO_NOMBRE, insertNombre.getText().toString());
            values.put(Usuario.CAMPO_TELEFONO, insertTelefono.getText().toString());

            if (foto!=null) {
                byte[] blob;
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                this.foto.compress(Bitmap.CompressFormat.JPEG, 100, out);
                blob = out.toByteArray();
                values.put(Usuario.CAMPO_FOTO, blob);
            }

            Long id = db.insert(Usuario.TABLA_USUARIOS, Usuario.CAMPO_ID, values);
            db.close();

            Toast.makeText(this, "Se insertó el nuevo elemento correctamente", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

        } else if (view.getId() == imagen.getId()) {

            // si NO tiene permiso
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                // pedir permiso
                String[] permisos = {Manifest.permission.READ_EXTERNAL_STORAGE};
                ActivityCompat.requestPermissions(MainActivity2.this, permisos, REQUEST_CODE_STORAGE_PERMISSION);

                // si tiene permiso
            } else {

                // seleccionar imagen
                seleccionarImagen();
            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // si es el permiso que yo solicité y si hubo resultados de esa solicitud
        if (requestCode == REQUEST_CODE_STORAGE_PERMISSION && grantResults.length>0) {

            // si dieron permiso
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                seleccionarImagen();
            } else {
                Toast.makeText(this,"Permiso denegado", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private static final int REQUEST_CODE_STORAGE_PERMISSION = 1;
    private static final int REQUEST_CODE_SELECT_IMAGE = 2;

    private void seleccionarImagen() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE);
        }

    }

    Bitmap foto;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK) {

            if (data!=null && data.getData()!=null) {

                try {

                    Uri selectImageUri = data.getData();
                    InputStream in = getContentResolver().openInputStream(selectImageUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(in);

                    imagen.setImageBitmap(bitmap);
                    this.foto = bitmap;

                } catch (FileNotFoundException e) {
                    Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }

        }
    }
}