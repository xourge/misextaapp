package udep.ing.poo.misextaapp;

import android.content.ContentValues;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import udep.ing.poo.misextaapp.db.Conexion;
import udep.ing.poo.misextaapp.db.Usuario;

public class MainActivity3 extends AppCompatActivity implements View.OnClickListener {

    EditText editId, editNombre, editTelefono;
    Conexion conexion;

    Button botonEditar, botonEliminar;
    ImageView imagen;

    byte[] blob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        conexion = new Conexion(this);

        editId = (EditText) findViewById(R.id.editId);
        editId.setEnabled(false);

        editNombre = (EditText) findViewById(R.id.editNombre);
        editTelefono = (EditText) findViewById(R.id.editTelefono);

        // recibo el objeto serializado
        Bundle extras = getIntent().getExtras();
        Usuario usuario = (Usuario)extras.getSerializable("usuario");

        editId.setText(usuario.getId()+"");
        editNombre.setText(usuario.getNombre());
        editTelefono.setText(usuario.getTelefono());

        botonEditar = (Button)findViewById(R.id.button4);
        botonEditar.setOnClickListener(this);

        botonEliminar = (Button)findViewById(R.id.button5);
        botonEliminar.setOnClickListener(this);

        imagen = (ImageView)findViewById(R.id.imageView2);
        imagen.setOnClickListener(this);

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Usuario.CAMPO_FOTO};
        String[] clave = {usuario.getId()+""};
        Cursor cursor = db.query(Usuario.TABLA_USUARIOS, campos, Usuario.CAMPO_ID+"=?",clave,null,null,null);
        cursor.moveToFirst();

        if (cursor.getBlob(0) != null) {
            blob = cursor.getBlob(0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(blob,0,blob.length);
            imagen.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onClick(View view) {

        SQLiteDatabase db = conexion.getWritableDatabase();
        String[] idUsuario = {editId.getText().toString()};

        if (view.getId() == botonEditar.getId()) {

            ContentValues values = new ContentValues();
            values.put(Usuario.CAMPO_NOMBRE, editNombre.getText().toString());
            values.put(Usuario.CAMPO_TELEFONO, editTelefono.getText().toString());

            db.update(Usuario.TABLA_USUARIOS, values, Usuario.CAMPO_ID+"=?",idUsuario);

            Toast.makeText(this, "Se actualizó el elemento correctamente", Toast.LENGTH_SHORT).show();

        }

        else if (view.getId() == botonEliminar.getId()) {

            db.delete(Usuario.TABLA_USUARIOS, Usuario.CAMPO_ID+"=?",idUsuario);
            Toast.makeText(this, "Se eliminó el elemento correctamente", Toast.LENGTH_SHORT).show();

        }

        else if (view.getId() == imagen.getId() && blob!=null) {

            File carpeta = null;

            try {

                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                carpeta = cw.getExternalFilesDir(null);
                File archivo = new File(carpeta, "perfil.jpg");

                FileOutputStream out = new FileOutputStream(archivo);
                out.write(blob);

            } catch (IOException e) {
                Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            if (carpeta!=null)
                Toast.makeText(this,"Se guardó la foto en " + carpeta.getAbsolutePath(),Toast.LENGTH_LONG).show();
        }

        db.close();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}